package org.yxj.pictureselectorlibaray;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import com.jbh.permissionlibrary.PermissionManager;
import com.jbh.permissionlibrary.base.BasePermissionActivity;
import com.jbh.permissionlibrary.i.CheckPermissionListener;
import com.jbh.permissionlibrary.i.PermissionRequestCode;
import org.yxj.pictureselectorlib.PictureSelector;
import org.yxj.pictureselectorlib.PictureSelectorDialog;
import org.yxj.pictureselectorlib.PictureSelectorListener;
import org.yxj.pictureselectorlib.PictureUtil;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BasePermissionActivity {

    private PictureSelector pictureSelector;
    private ImageView image ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = findViewById(R.id.image);

        pictureSelector = new PictureSelector();
        pictureSelector.setSupportCrop(true);
        pictureSelector.setListener(new PictureSelectorListener<String>() {
            @Override
            public void onPictureSelectorResult(String s) {
                Bitmap b = BitmapFactory.decodeFile(s);
                image.setImageBitmap(b);
            }
        });
    }



    public void pz(View view){
        new PictureSelectorDialog(this) {

            @Override
            public void setSelectorXC() {
                PermissionManager.newInstance().checkPermission(MainActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        PermissionRequestCode.WRITE_EXTERNAL_STORAGE_REQUEST_CODE
                        , new CheckPermissionListener() {
                            @Override
                            public void onNoNeedCheckPermission() {
                                pictureSelector.onPickPicture(MainActivity.this);
                            }
                        });
            }

            @Override
            public void setSelectorPZ() {
                List<String> list = new ArrayList<>();
                list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                list.add(Manifest.permission.CAMERA);
                PermissionManager.newInstance().checkPermission(MainActivity.this,
                        list,PermissionRequestCode.WRITE_EXTERNAL_STORAGE_AND_CAMERA_REQUEST_CODE,
                        new CheckPermissionListener() {
                            @Override
                            public void onNoNeedCheckPermission() {
                                pictureSelector.onTakePhoto(MainActivity.this,
                                        Environment.getExternalStorageDirectory().getPath());
                            }
                        });
            }
        }.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pictureSelector.onActivityResult(this,requestCode,resultCode,data);
    }
}
