# PictureSelectorLibaray

#### 介绍

![输入图片说明](https://images.gitee.com/uploads/images/2021/0914/161048_d47dc897_7852067.gif "SVID_20210914_160559_1-1.gif")


#### 软件架构
软件架构说明


#### 使用说明

1.  在项目根目录build.gradle

```
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```

2.  

```
dependencies {
	        implementation 'com.gitee.yxjme:PictureSelectorLibaray:v1.0.00'
    /**
     * 权限申请
     */
    implementation 'com.gitee.yxjme:CheckPermissionLibaray:v1.0.03'
	}
```

3.  AndroidManifest.xml文件

//所需权限
```

    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"
        tools:ignore="ProtectedPermissions" />
    <uses-permission android:name="android.permission.CAMERA"/>
```

//目前只支持AndroidX
```
       <provider
            android:name="androidx.core.content.FileProvider"
            android:authorities="${applicationId}.fileProvider"
            android:exported="false"
            android:grantUriPermissions="true">
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/file_paths" />
        </provider>
```
在res下创建xml文件夹，file_paths.xml文件

```
<?xml version="1.0" encoding="utf-8"?>
<paths>
    <external-path
        name="external_storage_root"
        path="." />
    <files-path
        name="files-path"
        path="." />
    <cache-path
        name="cache-path"
        path="." />
    <!--/storage/emulated/0/Android/data/...-->
    <external-files-path
        name="external_file_path"
        path="." />
    <!--代表app 外部存储区域根目录下的文件 Context.getExternalCacheDir目录下的目录-->
    <external-cache-path
        name="external_cache_path"
        path="." />
    <!--配置root-path。
    这样子可以读取到sd卡和一些应用分身的目录，
    否则微信分身保存的图片，就会导致 java.lang.IllegalArgumentException: Failed to find configured root that contains /storage/emulated/999/tencent/MicroMsg/WeiXin/export1544062754693.jpg，在小米6的手机上微信分身有这个crash，华为没有-->
    <root-path
        name="root-path"
        path="" />
</paths>
```

4、demo列子


```

public class MainActivity extends BasePermissionActivity {

    private PictureSelector pictureSelector;
    private ImageView image ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = findViewById(R.id.image);

        pictureSelector = new PictureSelector();
        pictureSelector.setSupportCrop(true);
        pictureSelector.setListener(new PictureSelectorListener() {
            @Override
            public void onPictureSelectorResult(Uri uri) {
                Bitmap b = BitmapFactory.decodeFile(PictureUtil.realFilePath(MainActivity.this, uri));
                image.setImageBitmap(b);
            }
        });
    }



    public void pz(View view){
        new PictureSelectorDialog(this) {

            @Override
            public void setSelectorXC() {
                PermissionManager.newInstance().checkPermission(MainActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        PermissionRequestCode.WRITE_EXTERNAL_STORAGE_REQUEST_CODE
                        , new CheckPermissionListener() {
                            @Override
                            public void onNoNeedCheckPermission() {
                                pictureSelector.onPickPicture(MainActivity.this);
                            }
                        });
            }

            @Override
            public void setSelectorPZ() {
                List<String> list = new ArrayList<>();
                list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                list.add(Manifest.permission.CAMERA);
                PermissionManager.newInstance().checkPermission(MainActivity.this,
                        list,PermissionRequestCode.WRITE_EXTERNAL_STORAGE_AND_CAMERA_REQUEST_CODE,
                        new CheckPermissionListener() {
                            @Override
                            public void onNoNeedCheckPermission() {
                                pictureSelector.onTakePhoto(MainActivity.this,
                                        Environment.getExternalStorageDirectory().getPath());
                            }
                        });
            }
        }.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pictureSelector.onActivityResult(this,requestCode,resultCode,data);
    }
```

