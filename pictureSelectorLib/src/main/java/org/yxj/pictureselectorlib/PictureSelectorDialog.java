package org.yxj.pictureselectorlib;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public abstract class PictureSelectorDialog extends Dialog {


    private LinearLayout rootContent ;
    private int buttonHeight = 50 ; //dp
    private int margin = 40 ; //px
    private int radius = 20 ; //px
    private Button btnCancel ;

    public PictureSelectorDialog(Context context) {
        super(context,R.style.dialog_0);
        setContentView(LayoutView(),new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        initWindow();
    }


    /**
     * 设置窗口显示的位置
     *
     */
    private void initWindow() {
        Window w = getWindow();
        w.setGravity(Gravity.CENTER|Gravity.BOTTOM);
        WindowManager.LayoutParams lp = w.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        w.setAttributes(lp);
    }

    /**
     * 设置显示的宽度
     *
     * @param wSize
     * @param hSize
     */
    public PictureSelectorDialog setSize(int wSize , int hSize){
        Window w = getWindow();
        w.setGravity(Gravity.CENTER|Gravity.BOTTOM);
        WindowManager.LayoutParams lp = w.getAttributes();
        lp.width = wSize;
        if (hSize!=0){
            lp.height = hSize;
        }
        w.setAttributes(lp);
        return PictureSelectorDialog.this;
    }


    /**
     * 显示view
     *
     * @return
     */
    private View LayoutView() {
        rootContent = new LinearLayout(getContext()) ;
        rootContent.setOrientation(LinearLayout.VERTICAL);
        /*添加按钮*/
        LinearLayout childView_1 = new LinearLayout(getContext());
        rootContent.addView(childView_1,new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        addButtonsView(childView_1);

        rootContent.addView(addBtnCancelView(),new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,buttonHeight,getContext().getResources().getDisplayMetrics())));

        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) btnCancel.getLayoutParams();;
        marginLayoutParams.bottomMargin=margin;
        marginLayoutParams.rightMargin=margin;
        marginLayoutParams.leftMargin=margin;
        return rootContent ;
    }


    /**
     * 取消按钮
     * @return
     */
    private View addBtnCancelView() {
        btnCancel = new Button(getContext()) ;
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(radius);
        gradientDrawable.setColor(Color.WHITE);
        btnCancel.setBackground(gradientDrawable);
        btnCancel.setTextColor(Color.BLACK);
        btnCancel.setText("取消");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return btnCancel ;
    }


    private void addButtonsView(LinearLayout childView_1) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(radius);
        gradientDrawable.setColor(Color.WHITE);
        childView_1.setBackground(gradientDrawable);
        childView_1.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.MarginLayoutParams marginLayoutParams= (ViewGroup.MarginLayoutParams) childView_1.getLayoutParams();
        marginLayoutParams.rightMargin = margin;
        marginLayoutParams.leftMargin = margin;
        marginLayoutParams.topMargin = margin;
        marginLayoutParams.bottomMargin = margin;
        /*添加相册选择按钮*/
        childView_1.addView(addBtnXCView(),new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,buttonHeight,getContext().getResources().getDisplayMetrics())));
        /*添加横线*/
        childView_1.addView(addBtnLineView(),new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,1,getContext().getResources().getDisplayMetrics())));
        /*添加拍照选择按钮*/
        childView_1.addView(addBtnPZView(),new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,buttonHeight,getContext().getResources().getDisplayMetrics())));
    }


    /**
     * 分割线
     *
     * @return
     */
    private View addBtnLineView() {
        View view = new View(getContext());
        view.setAlpha(0.5f);
        view.setBackgroundColor(Color.GRAY);
        return view ;
    }


    /**
     * 拍照
     * @return
     */
    private View addBtnPZView() {
        Button button = new Button(getContext()) ;
        button.setBackground(null);
        button.setTextColor(Color.BLACK);
        button.setText("拍照");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectorPZ();
                dismiss();
            }
        });
        return button ;
    }


    /**
     * 相册
     *
     * @return
     */
    private View addBtnXCView() {
        Button button = new Button(getContext()) ;
        button.setBackground(null);
        button.setTextColor(Color.BLACK);
        button.setText("从相册选择");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectorXC();
                dismiss();
            }
        });
        return button ;
    }


    public abstract void setSelectorXC();


    public abstract void setSelectorPZ();

}
