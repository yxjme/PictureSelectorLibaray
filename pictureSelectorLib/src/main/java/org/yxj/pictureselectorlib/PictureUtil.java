package org.yxj.pictureselectorlib;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class PictureUtil {





    /**
     * 将uri 转成 文件
     *
     * @param uri
     * @param context
     * @return
     */
    public static File uriToFile(Context context,Uri uri) {
        String path = null;
        if ("file".equals(uri.getScheme())) {
            path = uri.getEncodedPath();
            if (path != null) {
                path = Uri.decode(path);
                ContentResolver cr = context.getContentResolver();
                StringBuffer buff = new StringBuffer();
                buff.append("(").append(MediaStore.Images.ImageColumns.DATA).append("=").append("'" + path + "'").append(")");
                Cursor cur = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[] { MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA }, buff.toString(), null, null);
                int index = 0;
                int dataIdx = 0;
                for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                    index = cur.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    index = cur.getInt(index);
                    dataIdx = cur.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    path = cur.getString(dataIdx);
                }
                cur.close();
                if (index == 0) {
                } else {
                    Uri u = Uri.parse("content://media/external/images/media/" + index);
                    System.out.println("temp uri is :" + u);
                }
            }
            if (path != null) {
                return new File(path);
            }
        } else if ("content".equals(uri.getScheme())) {
            // 4.2.2以后
            String[] proj = { MediaStore.Images.Media.DATA };
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(columnIndex);
            }
            cursor.close();
            return new File(path);
        } else {
            //Log.i(TAG, "Uri Scheme:" + uri.getScheme());
        }
        return null;
    }




    /**
     * uri文件真实路径
     *
     * @param context
     * @param uri
     * @return
     */
    public static String realFilePath(Context context, Uri uri ) {
        if ( null == uri )
            return null;
        final String scheme = uri.getScheme();
        String data = null;
        if ( scheme == null )
            data = uri.getPath();
        else if ( ContentResolver.SCHEME_FILE.equals( scheme ) ) {
            data = uri.getPath();
        } else if ( ContentResolver.SCHEME_CONTENT.equals( scheme ) ) {
            Cursor cursor = context.getContentResolver().query( uri, new String[] {MediaStore.Images.ImageColumns.DATA
            }, null, null, null ); if ( null != cursor ) {
                if ( cursor.moveToFirst() ) {
                    int index = cursor.getColumnIndex( MediaStore.Images.ImageColumns.DATA );
                    if ( index > -1 ) { data = cursor.getString( index );
                    }
                }
                cursor.close();
            }
        }
        return data;
    }




    /**
     * 将图片添加到系统相册
     *
     * @param activity
     * @param path
     */
    public static Uri imgInsertToXc(Activity activity,String path){
        try {
            String u = MediaStore.Images.Media.insertImage(
                    activity.getContentResolver(),
                    path,
                    "",
                    "");

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                //这里主要进行了一下判断呢  在高版本的时候通知系统重重新扫描文件
//                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                Uri contentUri = Uri.parse(u); //out is your output file
//                mediaScanIntent.setData(contentUri);
//                activity.sendBroadcast(mediaScanIntent);
//            }else {
//                Intent intent = new Intent();
//                intent.setAction(Intent.ACTION_MEDIA_MOUNTED);
//                intent.setData(Uri.parse(u));
//                activity. sendBroadcast(intent);
//            }
            return Uri.parse(u);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null ;
    }




    /**
     * 裁剪
     *
     * @param uri
     */
    public static void cutPicture(Activity context, Uri uri) {
        UCrop.Options options = new UCrop.Options();
        File file=new File(context.getExternalCacheDir(), "uCrop.jpg");
        try {
            if (file.exists()){
                file.delete() ;
            }
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //裁剪后图片保存在文件夹中
        Uri destinationUri = Uri.fromFile(file);
        //第一个参数是裁剪前的uri,第二个参数是裁剪后的uri
        UCrop uCrop = UCrop.of(uri, destinationUri);
        //设置裁剪框的宽高比例
        uCrop.withAspectRatio(1,1);
        //下面参数分别是缩放,旋转,裁剪框的比例
        options.setAllowedGestures(UCropActivity.ALL,UCropActivity.NONE,UCropActivity.ALL);
        //设置标题栏文字
        options.setToolbarTitle("移动和缩放");
        //设置裁剪网格线的宽度(我这网格设置不显示，所以没效果)
        options.setCropGridStrokeWidth(2);
        //设置裁剪框的宽度
        options.setCropFrameStrokeWidth(2);
        //设置最大缩放比例
        options.setMaxScaleMultiplier(3);
        //隐藏下边控制栏
        options.setHideBottomControls(true);
        //设置是否显示裁剪网格
        options.setShowCropGrid(true);
        //设置是否为圆形裁剪框
//        options.setOvalDimmedLayer(true);
        //设置是否显示裁剪边框(true为方形边框)
        options.setShowCropFrame(true);
        //要能自定义调节裁剪框大小
        options.setFreeStyleCropEnabled(true);
        //剪裁图片的质量
//        options.setCompressionQuality(100);
        //一共三个参数，分别对应裁剪功能页面的“缩放”，“旋转”，“裁剪”界面，对应的传入NONE，就表示关闭了其手势操作，
        // 比如这里我关闭了缩放和旋转界面的手势，只留了裁剪页面的手势操作
        options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.SCALE, UCropActivity.ALL);
        //标题字的颜色以及按钮颜色
        options.setToolbarWidgetColor(Color.parseColor("#ffffff"));
        //设置裁剪外颜色
        options.setDimmedLayerColor(Color.parseColor("#AA000000"));
        // 设置标题栏颜色
        options.setToolbarColor(Color.parseColor("#000000"));
        //设置状态栏颜色
        options.setStatusBarColor(Color.parseColor("#000000"));
        //设置裁剪网格的颜色
        options.setCropGridColor(Color.YELLOW);
        //设置裁剪框的颜色
        options.setCropFrameColor(Color.YELLOW);
        //设置图片格式
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        //设置裁剪图片的最大尺寸
//        options.setMaxBitmapSize(100);
        uCrop.withOptions(options);
        uCrop.start(context);
    }
}
