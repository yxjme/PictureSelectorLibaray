package org.yxj.pictureselectorlib;

import android.net.Uri;

public interface PictureSelectorListener<T> {
    void onPictureSelectorResult(T t);
}
