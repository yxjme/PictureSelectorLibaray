package org.yxj.pictureselectorlib;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import static android.net.Uri.fromFile;



/**
 * 图片和拍照选择器
 */
public class PictureSelector {

    /**
     * 拍照存储的位置
     */
    private String takePhoto = "XJ_TakePhoto";
    /**
     * 拍照返回的uri
     */
    private Uri takePictureUri ;
    /**
     * 拍照图片的路径
     */
    private String takePicturePath ;
    /**
     * 是否支持剪切
     */
    private boolean isSupportCrop = false;


    public PictureSelector setSupportCrop(boolean isSupportCrop){
        this.isSupportCrop =isSupportCrop;
        return this;
    }


    public PictureSelector(){

    }


    /**
     * 回调选中的获取的图
     */
    private PictureSelectorListener listener;

    public void setListener(PictureSelectorListener listener) {
        this.listener = listener;
    }

    /**
     * 冲相册中选择图片
     *
     * @param activity
     */
    public  void onPickPicture(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(intent, PictureSelectorConstant.SELECTOR_XC);
    }



    /**
     * 拍照
     *
     *
     * Build.VERSION.SDK_INT >= 24
     *
     *
     * @param activity
     *
     *
     * @param savePicPath 根目录
     *
     */
    public Uri onTakePhoto(Activity activity,String savePicPath) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            /*拍照存储的位置文件夹*/
            String p= savePicPath + File.separator + takePhoto;
            File files=new File(p);
            if(!files.exists()){
                files.mkdir();
            }

            File file = new File(files.getAbsolutePath()+File.separator+System.currentTimeMillis()+".jpg");

            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (Build.VERSION.SDK_INT >= 24) {
                takePictureUri = FileProvider.getUriForFile(
                        activity.getApplicationContext(), activity.getPackageName()+".fileProvider",  file);
            } else {
                takePictureUri = fromFile(file);
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, takePictureUri);
            activity.startActivityForResult(intent, PictureSelectorConstant.SELECTOR_PZ);

            takePicturePath = file.getPath() ;
            return takePictureUri;
        } else {
            Toast.makeText(activity,"您的设备不支持此功能！",Toast.LENGTH_LONG).show();
        }
        return null ;
    }



    /**
     * 处理拍照或相册获取的图片
     * @param requestCode   请求码
     * @param resultCode    结果码
     * @param data          Intent
     * @return
     *
     */
    public void onActivityResult(Activity activity,int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bitmap bm = null;
            Uri uri = null;
            Log.v("[onActivityResult]->","requestCode="+requestCode);
            switch (requestCode) {
                /**
                 * 相册中选取
                 */
                case PictureSelectorConstant.SELECTOR_XC:
                    uri = data.getData();
                    if (isSupportCrop){
                        PictureUtil.cutPicture(activity,uri);
                    }else {
                        if (listener!=null){
                            listener.onPictureSelectorResult(PictureUtil.realFilePath(activity,uri));
                        }
                    }
                    break;
                /**
                 * 拍照
                 */
                case PictureSelectorConstant.SELECTOR_PZ:
//                    uri = PictureUtil.imgInsertToXc(activity,takePicturePath);
                    uri = takePictureUri;
                    if (isSupportCrop){
                        PictureUtil.cutPicture(activity,uri);
                    }else {
                        if (listener!=null){
                            listener.onPictureSelectorResult(takePicturePath);
                        }
                    }
                    break;

                /**
                 * 剪切
                 */
                case UCrop.REQUEST_CROP:
                    uri=UCrop.getOutput(data);
                    if (listener!=null){
                        listener.onPictureSelectorResult(PictureUtil.realFilePath(activity,uri));
                    }
                    break;
            }
        }
    }
}
