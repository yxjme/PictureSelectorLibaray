package org.yxj.pictureselectorlib;


/**
 * 常量类
 *
 */
public interface PictureSelectorConstant {
    /**
     * 相册
     */
    int SELECTOR_XC = 0x111;
    /**
     * 相机拍照
     */
    int SELECTOR_PZ = 0x222;
    /**
     * 剪切图片
     */
    int SELECTOR_CROP = 0x333;
}
